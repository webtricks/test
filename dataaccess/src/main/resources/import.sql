CREATE TABLE events(id INT PRIMARY KEY, name VARCHAR(255));
INSERT INTO events VALUES(1, 'Build base');
INSERT INTO events VALUES(2, 'Build factory');
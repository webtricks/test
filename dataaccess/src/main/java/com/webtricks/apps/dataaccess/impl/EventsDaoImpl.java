package com.webtricks.apps.dataaccess.impl;

import com.webtricks.apps.dataaccess.EventsDao;
import com.webtricks.apps.datamodel.Event;
import com.webtricks.apps.datamodel.State;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class EventsDaoImpl extends AbstractDaoImpl<Integer, Event> implements EventsDao {

    protected EventsDaoImpl() {
        super(Event.class);
    }

    public void addEvent() {
        State state = new State();

    }

    public List<Event> getEvents() {

        CriteriaBuilder cBuilder = getEm().getCriteriaBuilder();
        CriteriaQuery<Event> q = cBuilder.createQuery(Event.class);
        Root<Event> root = q.from(Event.class);

        q.select(root);

        TypedQuery<Event> query = getEm().createQuery(q);

        List<Event> resultList = query.getResultList();
        return resultList;


    }
}

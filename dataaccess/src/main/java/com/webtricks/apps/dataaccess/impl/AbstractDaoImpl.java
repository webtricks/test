package com.webtricks.apps.dataaccess.impl;

import com.webtricks.apps.dataaccess.AbstractDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class AbstractDaoImpl<I, E> implements AbstractDao<I, E> {

    private final Class<E> entityClass;

    @PersistenceContext
    private EntityManager em;

    protected AbstractDaoImpl(final Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    EntityManager getEm() {
        return em;
    }

}

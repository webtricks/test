package com.webtricks.apps.dataaccess;

import com.webtricks.apps.datamodel.Event;

import java.util.List;

public interface EventsDao extends AbstractDao<Integer, Event> {

    void addEvent();

    List<Event> getEvents();

}

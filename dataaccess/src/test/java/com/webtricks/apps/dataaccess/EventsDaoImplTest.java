package com.webtricks.apps.dataaccess;

import com.webtricks.apps.dataaccess.config.DaoConfig;
import com.webtricks.apps.datamodel.Event;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DaoConfig.class})
@WebAppConfiguration
public class EventsDaoImplTest {

    @Inject
    private EventsDao eventsDao;

    @Test
    @Ignore
    public void test() {
        List<Event> events = eventsDao.getEvents();
        assertEquals(events.size(), 2);
    }

}

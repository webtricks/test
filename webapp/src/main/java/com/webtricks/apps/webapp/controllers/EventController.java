package com.webtricks.apps.webapp.controllers;

import com.webtricks.apps.services.EventService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

import static com.webtricks.apps.webapp.Const.JSON;
import static com.webtricks.apps.webapp.Const.Rest.EVENT;

@RestController
public class EventController {

    @Inject
    private EventService eventService;

    @RequestMapping(value = EVENT, method = RequestMethod.POST, produces = JSON)
    public String addEvent() {
        eventService.addEvent();
        return "0";
    }

}

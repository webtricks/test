package com.webtricks.apps.webapp;

public class Const {

    public static class Rest {

        public static final String EVENT = "/event";

        private Rest() {
        }

    }

    public static final String JSON = "application/json;charset=UTF-8";

    private Const() {
    }
}